export function isNullOrWhiteSpaceOrUndefined(str) {
  if (
    typeof str === 'undefined' ||
    str === undefined ||
    str === null ||
    str.toString().toLowerCase() === 'undefined' ||
    str.toString().toLowerCase() === 'null' ||
    str === '' ||
    str.toString().replace(/(^\s*)|(\s*$)/g, '').length === 0
  ) {
    return true
  }
  return false
}
export function isEqual(o1, o2) {
  if (o1 === o2) {
    return true
  }
  if ((typeof o1 === 'undefined' || typeof o2 === 'undefined') && typeof o1 !== typeof o2) {
    return false
  } else {
    if (o1.toString().toLowerCase() === o2.toString().toLowerCase()) {
      return true
    }
  }

  return false
}
