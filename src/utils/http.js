import axios from 'axios'

// 创建axios实例
const service = axios.create({
  baseURL: process.env.BKLINK_API, // api的base_url
  timeout: 60000, // 请求超时时间
  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
})

// respone拦截器
service.interceptors.response.use(
  response => {
    console.debug('vue.response', response)
    return response
  },
  error => {
    console.error('vue.error', error)
    return Promise.reject(error)
  }
)
export default service
