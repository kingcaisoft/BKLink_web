import request from '@/utils/http'
import qs from 'qs'

/**
 * 查询模板列表
 *
 * @export
 * @returns {jsonarry} 模板单据头列表
 */
export function getTempList() {
  return request({
    url: '/bk/template/getlist',
    method: 'get'
  })
}
/**
 * 根据模板ID，查询模板信息
 *
 * @export
 * @param {string} id  模板Id
 * @returns {jsonobject}
 */
export function getTemplate(id) {
  var data = { tempId: id }
  return request({
    url: '/bk/template/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 查询模板明细
 *
 * @export
 * @param {int} id 模板ID
 * @returns {jsonarray} 模板明细列表
 */
export function getTempDetail(id) {
  var data = { tempId: id }
  return request({
    url: '/bk/template/getdetail',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 查询模板实体
 *
 * @export
 * @param {int} id 模板ID
 * @returns {jsonarray} 模板实体列表
 */
export function getTempEntity(id) {
  var data = { tempId: id }
  return request({
    url: '/bk/template/getentity',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 保存模板单据头
 *
 * @export
 * @param {string} templateStr 模板单据头jsonobject字符串
 * @returns {int} 保存的行数
 */
export function saveTemplate(templateStr) {
  var data = { template: templateStr }
  return request({
    url: '/bk/template/save',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 保存模板明细
 *
 * @export
 * @param {string} detailsStr 模板明细jsonarray字符串
 * @returns {int} 保存的行数
 */
export function saveTemplateDetails(detailsStr) {
  var data = { details: detailsStr }
  return request({
    url: '/bk/template/savedetail',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 查询金蝶版本列表
 *
 * @export
 * @returns
 */
export function getKdVersions() {
  return request({
    url: '/kd/version/getall',
    method: 'get'
  })
}
/**
 * 查询金蝶单据模板列表
 *
 * @export
 * @param {string} keyword 关键字
 * @returns {jsonarray}
 */
export function getKdBillTemplates(keyword) {
  var data = { key: keyword }
  return request({
    url: '/kd/template/querybykey',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 根据单据标识，查询金蝶单据模板
 *
 * @export
 * @param {string} formId 关键字
 * @returns {jsonobject}
 */
export function getKdBillTemplateByFormId(id) {
  var data = { formId: id }
  return request({
    url: '/kd/template/queryKdTemplateByFormId',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 查询金蝶单据字段列表，根据金蝶模板ID
 *
 * @export
 * @param {string} keyword 关键字
 * @returns {jsonarray}
 */
export function getKdBillTemplateDetail(tempid) {
  var data = { kdTempId: tempid }
  return request({
    url: '/kd/template/querydetail',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取工单模板列表
 *
 * @export
 * @returns {jsonarray}
 */
export function getBwbTicketTemplates() {
  return request({
    url: '/bangwo8/template/queryTicketTemplates',
    method: 'get'
  })
}
/**
 * 根据名称,获取工单模板列表
 *
 * @export
 * @param {string} keyword 关键字
 * @returns {jsonarray}
 */
export function getBwbTicketTemplatesByName(kewword) {
  var data = { name: kewword }
  return request({
    url: '/bangwo8/template/queryTicketTemplatesByName',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 根据id,获取工单模板
 *
 * @export
 * @param {string} keyword 工单模板ID
 * @returns {jsonobject}
 */
export function getBwbTicketTemplatesById(kewword) {
  var data = { tempId: kewword }
  return request({
    url: '/bangwo8/template/queryTicketTemplatesById',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取指定工单模板的自定义字段列表
 *
 * @export
 * @param {string} tempid 模板ID
 * @returns {jsonarray}
 */
export function getBwbTicketTemplateDetail(tempid) {
  var data = { ticketTemplateId: tempid }
  return request({
    url: '/bangwo8/template/queryTicketFields',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 根据核算项目表名，查询字段列表
 *
 * @export
 * @param {*} formId
 * @returns
 */
export function getKdBaseItemFields(formId) {
  var data = { formId: formId }
  return request({
    url: '/kd/template/queryKdBaseItemFields',
    method: 'post',
    data: qs.stringify(data)
  })
}
