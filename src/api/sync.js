import request from '@/utils/http'
import qs from 'qs'

/**
 * 根据帮我吧工单ID同步单据到金蝶
 *
 * @export
 * @param {*} id 工单ID
 * @param {*} type 0 保存，1 修改
 * @returns
 */
export function savebill(id, type) {
  var data = { ticketId: id, type: 0 }
  // 创建axios实例
  return request({
    url: '/bangwo8/ticket/save',
    method: 'post',
    data: qs.stringify(data)
  })
}

export function hello(id, type) {
  var data = { ticketId: id, type: 0, name: 'BKLink' }
  return request({
    url: '/test/hellojson',
    method: 'post',
    data: qs.stringify(data)
  })
}
